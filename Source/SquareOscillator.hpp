//
//  SquareOscillator.hpp
//  JuceBasicAudio
//
//  Created by Callum McNeill on 30/01/2018.
//
//

#ifndef H_SQUAREOSCILLATOR
#define H_SQUAREOSCILLATOR

#include <stdio.h>
#include "Oscillator.hpp"

class SquareOscillator : public Oscillator
{
public:
    SquareOscillator();
    ~SquareOscillator();
    
    float renderWaveShape (const float currentPhase) override;
    
private:
};

#endif //H_SQUAREOSCILLATOR
