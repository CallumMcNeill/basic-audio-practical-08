/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) : audio (a)
{
    setSize (500, 400);

    //addAndMakeVisible(waveFormSelector);
    //waveFormSelector.setText("Select Waveform");
    //waveFormSelector.addItem ("Sine wave", 1);
    //waveFormSelector.addItem ("Square wave", 2);
    
    addAndMakeVisible(textButton1);
    textButton1.setButtonText("Sin Wave");
    textButton1.addListener(this);
    
    addAndMakeVisible(textButton2);
    textButton2.setButtonText("Square Wave");
    textButton2.addListener(this);
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    //waveFormSelector.setBounds(20, 20, getWidth(), 20);
    textButton1.setBounds(0, 20, getWidth(), 40);
    textButton2.setBounds(0, 80, getWidth(), 40);
}

//void MainComponent::comboBoxChanged (ComboBox *comboBoxThatHasChanged)
//{
//    if (comboBoxThatHasChanged == &waveFormSelector)
//    {
//    if (comboBoxThatHasChanged->getSelectedId() == 1)
//    {
//        audio.readSinOscillatorPointer(oscillatorPointer.get());
//        DBG("sin item selected");
//    }
//    else if (comboBoxThatHasChanged->getSelectedId() == 2)
//    {
//        audio.readSquareOscillatorPointer(oscillatorPointer.get());
//        DBG("square item selected");
//    }
//    }
//}

void MainComponent::buttonClicked (Button* button)
{
    DBG ("Button Clicked\n");
    if (button == &textButton1)
    {
        DBG ("textButton1 clicked!\n");
        audio.setPointerToSin();
    }
    else if (button == &textButton2)
    {
        DBG ("textButton2 clicked!\n");
        audio.setPointerToSquare();
    }
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

