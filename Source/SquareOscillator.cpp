//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Callum McNeill on 30/01/2018.
//
//

#include "SquareOscillator.hpp"

SquareOscillator::SquareOscillator(){
    
}

SquareOscillator::~SquareOscillator(){
    
}

float SquareOscillator::renderWaveShape (const float currentPhase)
{
    return square (currentPhase);
}