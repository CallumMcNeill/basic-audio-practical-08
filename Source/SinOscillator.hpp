/*
 *  SinOscillator.h
 *  sdaAudioMidi
 *
 *  Created by tjmitche on 11/11/2010.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef H_SINOSCILLATOR
#define H_SINOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"

/**
 Class for a sinewave oscillator
 */

class SinOscillator : public Oscillator
{
public:
    SinOscillator();
    ~SinOscillator();
    
    float renderWaveShape (const float currentPhase) override;
    
private:
};

#endif //H_SINOSCILLATOR