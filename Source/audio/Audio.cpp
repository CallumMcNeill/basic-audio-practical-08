/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    frequency = message.getMidiNoteInHertz (message.getNoteNumber());
}

void Audio::setPointerToSin()
{
    oscillatorPointer = &sinOscillator;
}

void Audio::setPointerToSquare()
{
    oscillatorPointer = &squareOscillator;
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];

    
    while(numSamples--)
    {
        *outL = *inL;
        *outR = *inR;
        
        float mix = oscillatorPointer.get()->nextSample();
        *outL = mix;
        *outR = mix;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    oscillatorPointer.get()->setSampleRate(device->getCurrentSampleRate());
}

void Audio::audioDeviceStopped()
{

}